# editor

editor for ipfs mutable file system

description

> **this is not an implementation of an entire ipfs node**, this project only use the `api` that the _daemon_ binds over the port `5001` (by default). [go](https://github.com/ipfs/go-ipfs) or [javascript](https://github.com/ipfs/js-ipfs) implementation is by your own.

## ip[fn]s links
* [/ipns/](https://ipfs.io/ipns/) (permanent)
* [/ipfs/](https://ipfs.io/ipfs/) latest

## technologies

* [reactjs](https://reactjs.org)
* [ipfs](https://ipfs.io)

## dev

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
