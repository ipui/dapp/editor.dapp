import React, { useState } from 'react'
import { withFile } from '../file'
import StatusBar from '../statusBar'
import IpfsConnect from '../ipfsConnect'
import Rename from '../rename'

import { FiSettings, FiX } from 'react-icons/fi'

function TopBar( props ) {
  const { filename } = props.withFile
  const [ isToggled, setIsToggled ] = useState( false )

  function toggle() {
    setIsToggled( !isToggled )
  }

  return (
    <>
      <header>
        { !isToggled ? (
          <>
            <h1>{ filename || 'untitled' }</h1>
            <StatusBar />
          </>
        ) : <Rename close={ toggle } /> }

        <button onClick={ toggle }>
          { isToggled ? ( <FiX /> ) : ( <FiSettings /> ) }
        </button>
      </header>

      { isToggled ? (
        <header>
          <IpfsConnect />
        </header>
      ) : null }
    </>
  )
}

export default withFile( TopBar )
