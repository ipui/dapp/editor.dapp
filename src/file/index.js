import React, { createContext, Component } from 'react'
import RouteTools from '../lib/RouteTools'
import Iterator from '../lib/Iterator'
import { withIpfs } from '../ipfs'
import Status, { withStatus } from '../status'
import PropTypes from 'prop-types'

const FileContext = createContext()

class FileInceptionError extends Error {
  constructor( message ) {
    super( message )
    this.name = 'FileInceptionError'
  }
}

class File extends Component {

  static propTypes = {
    autoSaveDelay: PropTypes.number
  }

  timeout = null

  state = {
    source: undefined,
    fullpath: undefined,
    dirname: undefined,
    filename: undefined,
    stats: undefined,
    editorOptions: {
      mode: undefined,
      keyMap: 'vim'
    }
  }

  constructor( props ) {
    super( props )
    this.load = this.load.bind( this )
    this.update = this.update.bind( this )
    this.save = this.save.bind( this )
    this.rename = this.rename.bind( this )
    this.forceSave = this.forceSave.bind( this )
    this.setEditorOptions = this.setEditorOptions.bind( this )
  }

  load( path ) {
    const { files } = this.props.withIpfs.node

    files.stat( path )
      .then( stats => {
        const { type } = stats

        switch( type ) {
          case 'directory':
            this.setState( ( state, props ) => ( {
              ...state,
              fullpath: path,
              dirname: path,
              stats
            } ) )
            break

          case 'file':
            this.setState( ( state, props ) => ( {
              ...state,
              fullpath: path,
              dirname: RouteTools.dirname( path ),
              filename: RouteTools.filename( path ),
              stats
            } ) )
            this.loadFile( path )
            break

          default:
            console.error( 'unhandle type' )
        }
      } )
      .catch( err => {
        this.touch( path )
      } )
  }

  async loadFile( path ) {
    const { files } = this.props.withIpfs.node

    const fileIterator = files.read( path )

    const sourceArray = await Iterator.reduce( fileIterator, ( val ) => {
      return Buffer.from( val ).toString( 'utf8' )
    } )

    this.setState( ( state, props ) => ( {
      ...state,
      source: sourceArray.join()
    } ) )
  }

  async touch( path ) {
    const dirname = RouteTools.dirname( path )
    const { files } = this.props.withIpfs.node

    files.stat( dirname )
      .then( stats => {
        if ( stats.type !== 'directory' )
          throw new FileInceptionError( 'folder name must be a directory.' )

        return files.write( path, Buffer.from( '' ), {
          create: true,
          truncate: true,
          parents: true
        } )
      } )
      .then( () => {
        this.load( path )
      } )
      .catch( async err => {
        if ( err instanceof FileInceptionError ) {
          RouteTools.go( dirname )
          this.load( dirname )

        } else if ( err.name === 'HTTPError' ) {
          const message = err.message.toLowerCase()

          if ( message.endsWith( 'not a directory' ) ) {
            RouteTools.go( dirname )
            this.load( dirname )

          } else if ( message === 'file does not exist' ) {
            await files.write( path, Buffer.from( '' ), {
              create: true,
              truncate: true,
              parents: true
            } )

            this.load( path )

          } else {
            console.error( 'unhandled HTTPError', err.message )
          }

        } else {
          console.error( 'unhandled error', err )
        }
      } )
  }

  save() {
    const { files } = this.props.withIpfs.node
    const { fullpath, source, stats } = this.state

    if ( !stats )
      return console.error( 'save is not allowed' )

    if ( stats.type !== 'file' ) return console.error( 'prevent save' )

    files.write( fullpath, Buffer.from( source ), {
      crate: true,
      truncate: true
    } )
      .then( () => {
        this.props.withStatus.update( Status.SAVED )
      } )
      .catch( err => {
        console.error( err )
      } )
  }

  forceSave() {
    if ( !this.timeout ) return

    clearTimeout( this.timeout )

    this.save()
  }

  waitSave() {
    const { autoSaveDelay = 6000 } = this.props
    if ( this.timeout ) clearTimeout( this.timeout )

    this.props.withStatus.update( Status.REFRESH )

    this.timeout = setTimeout( this.save, autoSaveDelay )
  }

  update( source ) {
    this.setState( ( state, props ) => ( {
      ...state,
      source
    } ) )

    this.waitSave()
  }

  async rename( newName ) {
    const { files } = this.props.withIpfs.node
    const { fullpath, dirname, stats, source } = this.state

    let newpath

    switch ( stats.type ) {
      case 'directory':
        newpath = RouteTools.join( dirname, newName )
        await files.write( newpath, Buffer.from( source ), {
          create: true,
          truncate: true
        } )
        break

      case 'file':
        newpath = RouteTools.join( dirname, newName )
        await files.mv( fullpath, newpath )
        break

      default:
        console.error( 'unhandle object type' )
    }

    RouteTools.go( newpath )
    this.load( newpath )
  }

  setEditorOptions( options = {} ) {
    this.setState( ( state, props ) => ( {
      ...state,
      editorOptions: {
        ...state.editorOptions,
        ...options
      }
    } ) )
  }

  componentDidMount() {
    const { addresses } = RouteTools.parse()
    this.load( addresses.app )
  }

  render() {
    const { children } = this.props
    const { filename, dirname, source, editorOptions } = this.state
    const { update, rename, forceSave, setEditorOptions } = this

    return (
      <FileContext.Provider value={ {
        filename,
        dirname,
        source,
        editorOptions,
        update,
        rename,
        forceSave,
        setEditorOptions
      } } >
        { children }
      </FileContext.Provider>
    )
  }
}

const withFile = ( ComponentAlias ) => {
  return props => (
    <FileContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withFile={ context } />
      } }
    </FileContext.Consumer>
  )
}

export default withIpfs( withStatus( File ) )

export {
  FileContext,
  withFile
}
