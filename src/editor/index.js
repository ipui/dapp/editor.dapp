import React, { Component } from 'react'
import { withFile } from '../file'
import { Controlled as CodeMirror } from 'react-codemirror2'
import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/base16-dark.css'
import 'codemirror/keymap/vim'

class Editor extends Component {

  render() {
    const { source, update, editorOptions } = this.props.withFile
    const { mode, keyMap } = editorOptions

    return (
      <main>
        <CodeMirror
          value={ source }
          options={ {
            keyMap,
            mode,
            theme: 'base16-dark',
            lineNumbers: true
          } }
          onBeforeChange={ ( editor, data, val ) => {
            update( val )
          } }
        />
      </main>
    )
  }
}

export default withFile( Editor )
