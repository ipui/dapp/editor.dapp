import React, { useState } from 'react'
import { withFile } from '../file'
import PropTypes from 'prop-types'

import { FiCheck } from 'react-icons/fi'

function Rename( props ) {
  const { filename, rename } = props.withFile
  const [ name, setName ] = useState( '' )

  function updateInput( e ) {
    setName( e.target.value )
  }

  function action() {
    rename( name )
    props.close()
  }

  function isEnter( e ) {
    if ( e.keyCode !== 13 ) return

    e.preventDefault()
    action()
  }

  return (
    <>
      <input
        type="text"
        value={ name }
        onChange={ updateInput }
        onKeyDown={ isEnter }
        placeholder={ filename || 'untitled' }
        size="1"
      />

      <button onClick={ action }>
        <FiCheck />
      </button>
    </>
  )
}

Rename.propTypes = {
  close: PropTypes.func.isRequired
}

export default withFile( Rename )
