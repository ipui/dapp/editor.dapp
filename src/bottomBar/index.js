import React from 'react'
import GoToDir from '../goToDir'
import SelectMode from '../selectMode'

function BottomBar( props ) {

  return (
    <footer className="space-between">
      <GoToDir />

      <div>
        <SelectMode />
      </div>
    </footer>
  )
}

export default BottomBar
